# Restaurant App
Application for account office, based on MVVM pattern, using tools / technology:
 - Caliburn Micro
 - WPF
 - Xceed Wpf Toolkit
 - Git

![enter image description here](https://i.ibb.co/37fWZfT/full-Size-View.jpg)


## Project Structure
```
├───Exceptions
├───images
│   └───backgrounds
├───Models
├───Repository
│   ├───impl
│   └───testData
├───Service
│   ├───impl
│   └───util
├───Styles
├───ViewModels
│   └───Events
└───Views
```


## Description

###    The main purpose of the application is to provide the users information about the current billing amount based on the number of documents.

#### Application gives you the option:
- add a new company, set rates and annotations  for it
-   check the details of the rates, edit them
- check the annotations, edit them
- remove companies

### Simple View 
1. Select Company.
2.   Enter the number of documents and get the price.

![enter image description here](https://i.ibb.co/rZ1TYCs/simpleview.jpg)
  
  ### Details View 
#### In the Details View, there are options for quick / advanced rate editing, adding / editing annotations and deleting a company
![enter image description here](https://i.ibb.co/myCd8KS/details-View.jpg)
### About Rates
#### The company have standard rates or one constant rate
### Rates Editor View

#### In the rate editor you can freely change the data, add additional rates. There is also the option of setting company rates exactly the same as for another company (deep copy).

![enter image description here](https://i.ibb.co/NSMr3q4/rates-Editor-View.jpg)

### Add Next Rate View
####   Here you can add next rate, you can also change standard rates <-> constant rates
![enter image description here](https://i.ibb.co/RHvTcD4/addrate-View.jpg)

### Exceptions
The application has error handling and data validation

