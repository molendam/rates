﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Service.util
{
    class RateComparer : IComparer<RateModel>
    {
        public int Compare(RateModel x, RateModel y)
        {
            return x.LowerLimit.CompareTo(y.LowerLimit);
        }
    }
}
