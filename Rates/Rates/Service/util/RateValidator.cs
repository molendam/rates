﻿using Rates.Exceptions;
using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Service.util
{
    static class RateValidator
    {
        public static bool IsValidRate(RateModel rate)
        {
            //validation of const rate - only price 
            
            if (rate.ConstRate)
            {
                ConstRateRangeValidator(rate);
            }
            else
            {
                RegularRateRangeValidator(rate);
            }
            return true;
        }

        private static void ConstRateRangeValidator(RateModel rate)
        {
            PriceValidator(rate.Price);
            if (rate.LowerLimit!=0 ||rate.UpperLimit!=0)
            {
                throw new RateException(ExceptionMsg._errMsg14);
            }
        }

        private static bool PriceValidator(double price)
        {
            return price > 0 ? true : throw new RateException(ExceptionMsg._errMsg7);
        }
        private static bool RegularRateRangeValidator(RateModel rate)
        {
            PriceValidator(rate.Price);
            if (rate.UpperLimit<=0 || rate.LowerLimit <= 0) {
                throw new RateException(ExceptionMsg._errMsg13);
            } 
            return rate.UpperLimit > rate.LowerLimit ? true : throw new RateException(ExceptionMsg._errMsg6);
        }
    }
}
