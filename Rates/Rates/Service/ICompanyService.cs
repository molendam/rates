﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Service
{
    public interface ICompanyService
    {
        CompanyModel AddCompany(string name, IList<RateModel> rates, string annotations);
        void DeleteCompany(string name);
        CompanyModel FindCompany(string name);
        void UpdateCompanyRates(string companyToUpdateName,  IList<RateModel> rates);
        void UpdateCompanyAnnotations(string companyToUpdateName,  string annotations);
        IList<CompanyModel> GetAll();
    }
}
