﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Service
{
    interface ICompanyRateService
    {
        RateModel FindRate(int numberOfDocuments);
        void DeleteRates();
        void AddOrUpdateConstRate(RateModel rate);
        void AddOrUpdateRegularRates(IList<RateModel> rates);
        void AddNextRegularRate(RateModel nextRate);
        bool RatesValidator(IList<RateModel> rates);
        CompanyModel GetCompany();
        IList<RateModel> GetAllRates();
        void AddOrUpdateRates(IList<RateModel> rates);
    }
}
