﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Service
{
    public interface IRateService
    {
        RateModel CreateConstRate(double price);
        RateModel CreateRegularRate(int lowerLimit, int upperLimit, double price);
    }
}
