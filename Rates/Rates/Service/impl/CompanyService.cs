﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Rates.Exceptions;
using Rates.Models;
using Rates.Repository;
using Rates.Repository.impl;
using Rates.Service.impl;
using Rates.Service.util;

namespace Rates.Service
{
    class CompanyService : ICompanyService
    {
        private ICompanyRepository companyRepository;
        private ICompanyRateService companyRateService;

        public CompanyService(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;

        }
        public CompanyModel AddCompany(string name, IList<RateModel> rates, string annotations)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new CompanyException(ExceptionMsg._errMsg5);
            }
            if (rates == null || rates.Count == 0)
            {
                throw new CompanyException(ExceptionMsg._errMsg12);
            }
            CompanyModel companyModel = companyRepository.FindCompany(name);
            if (companyModel == null)
            {
                Console.WriteLine(annotations);
                return companyRepository.AddCompany(name, rates, annotations);
            }
            throw new CompanyException(ExceptionMsg._errMsg3);
        }
        public void DeleteCompany(string name)
        {
            companyRepository.DeleteCompany(name);
        }
        public CompanyModel FindCompany(string name)
        {
            CompanyModel company = companyRepository.FindCompany(name);
            return company == null ? throw new CompanyException(ExceptionMsg._errMsg1) : (CompanyModel)company.Clone();
        }
        public IList<CompanyModel> GetAll()
        {
            List<CompanyModel> repoCompanies = companyRepository.GetAll().ToList();
            repoCompanies.Sort();
            IList<CompanyModel> cloneCompanies = repoCompanies.ConvertAll(company => (CompanyModel)company.Clone());
            return cloneCompanies;
        }

        public void UpdateCompanyAnnotations(string companyToUpdateName, string annotations)
        {
            CompanyModel companyModel = companyRepository.FindCompany(companyToUpdateName);
            if (companyModel != null)
            {
                companyModel.Annotations = annotations;
            }
        }

        public void UpdateCompanyRates(string companyToUpdateName, IList<RateModel> rates)
        {
            CompanyModel repoCompany = companyRepository.FindCompany(companyToUpdateName);
            if (repoCompany == null)
            {
                throw new CompanyException(ExceptionMsg._errMsg1);
            }
            companyRateService = new CompanyRateService(repoCompany);

            if (rates.Count > 1)
            {
                companyRateService.AddOrUpdateRegularRates(rates);
            }
            else
            {
                companyRateService.AddOrUpdateConstRate(rates[0]);
            }
        }
    }
}
