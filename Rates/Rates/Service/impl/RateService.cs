﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rates.Exceptions;
using Rates.Models;
using Rates.Service.util;

namespace Rates.Service.impl
{
    class RateService : IRateService
    {
        public RateModel CreateConstRate(double price)
        {
            RateModel rate = new RateModel( price);
            return RateValidator.IsValidRate(rate) ?rate : throw new RateException(ExceptionMsg._errMsg9);
        }
        public RateModel CreateRegularRate(int lowerLimit, int upperLimit, double price)
        {
            RateModel rate =  new RateModel(lowerLimit, upperLimit, price);
            return RateValidator.IsValidRate(rate)? rate : throw new RateException(ExceptionMsg._errMsg9);
        }
    }
}
