﻿using Rates.Exceptions;
using Rates.Models;
using Rates.Repository;
using Rates.Repository.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rates.Service.util;

namespace Rates.Service.impl
{
    class CompanyRateService : ICompanyRateService
    {
        public IList<RateModel> Rates
        {
            get => _company.Rates; set
            {
                if (_company != null)
                {
                    _company.Rates = value;
                }
            }
        }
        private CompanyModel _company;
        public CompanyModel GetCompany() { return _company; }
        public CompanyRateService(CompanyModel company)
        {
            this.Rates = company == null ?  null : company.Rates;
            _company = company;
        }
        public RateModel FindRate(int numberOfDocuments)
        {

            //CompanyModel company = getCompany(companyName);
            //if const rate
            if (Rates.Count == 1 && Rates[0].ConstRate)
            {
                return Rates[0];
            }
            //else try find
            for (int i = 0; i < Rates.Count; i++)
            {
                RateModel rate = Rates[i];
                if (numberOfDocuments >= rate.LowerLimit &&
                    numberOfDocuments <= rate.UpperLimit)
                {
                    return rate;
                }
            }
            throw new RateException(ExceptionMsg._errMsg1);
        }
        public void AddOrUpdateConstRate(RateModel rate)
        {
            //CompanyModel company = getCompany(companyName);
            RateValidator.IsValidRate(rate);
            Rates.Clear();
            Rates.Add(rate);
        }
        public void AddOrUpdateRegularRates(IList<RateModel> rates)
        {
            //CompanyModel company = getCompany(companyName);
            List<RateModel> rateList = new List<RateModel>(rates);
            //RateComparer rc = new RateComparer();
            //rateList.Sort(rc);


            //for (int i = 0; i < rateList.Count; i++)
            //{
            //    if (!RateValidator.IsValidRate(rateList[i]))
            //    {
            //        throw new RateException(ExceptionMsg._errMsg10);
            //    }
            //    if (i > 0)
            //    {
            //        bool isCorrectRelation = NewRateHasCorrectRelationWithPreviousRate(rateList[i], rateList[i - 1]);
            //        if (!isCorrectRelation)
            //        {
            //            throw new RateException(ExceptionMsg._errMsg11);
            //        }
            //    }
            //}
            RatesValidator(rateList);
            Rates.Clear();
            Rates = Rates.Concat(rateList).ToList();

        }
        public void AddNextRegularRate(RateModel nextRate)
        {
            int ratesCount = Rates.Count;
            //czy tu będzie NullPointerExc?
            RateModel previousRate = ratesCount == 0 ? null : Rates[ratesCount - 1];
            bool isCorrectRelation = NewRateHasCorrectRelationWithPreviousRate(nextRate, previousRate);
            if (!isCorrectRelation)
            {
                throw new RateException(ExceptionMsg._errMsg11);
            }
            Rates.Add(nextRate);
        }
        public void DeleteRates()
        {
            Rates.Clear();
        }
        private bool NewRateHasCorrectRelationWithPreviousRate(RateModel newRate, RateModel previousRate)
        {
            if (previousRate == null || newRate.UpperLimit == 0)
            {
                return true;
            }
            if (newRate == null)
            {
                return false;
            }
            int upperLimitPreviousRate = previousRate.UpperLimit;
            int lowerLimitNextRate = newRate.LowerLimit;

            return upperLimitPreviousRate >= lowerLimitNextRate ?
                throw new RateException(ExceptionMsg._errMsg8) : true;
        }
        public IList<RateModel> GetAllRates()
        {
            return Rates;
        }

        public bool RatesValidator(IList<RateModel> rates)
        {
            if (rates.Count==0)
            {
                return true;
            }
            List<RateModel> rateList = new List<RateModel>(rates);
            RateComparer rc = new RateComparer();
            rateList.Sort(rc);


            for (int i = 0; i < rateList.Count; i++)
            {
                if (!RateValidator.IsValidRate(rateList[i]))
                {
                    throw new RateException(ExceptionMsg._errMsg10);
                }
                if (i > 0)
                {
                    bool isCorrectRelation = NewRateHasCorrectRelationWithPreviousRate(rateList[i], rateList[i - 1]);
                    if (!isCorrectRelation)
                    {
                        throw new RateException(ExceptionMsg._errMsg11);
                    }
                }
            }
            return true;
        }

        public void AddOrUpdateRates(IList<RateModel> rates)
        {
            if (RatesValidator(rates))
            {
                _company.Rates = rates;
            }
        }
     


    }
}