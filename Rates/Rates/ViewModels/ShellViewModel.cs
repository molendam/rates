﻿using Caliburn.Micro;
using Rates.Exceptions;
using Rates.Models;
using Rates.Repository.impl;
using Rates.Service;
using Rates.Service.impl;
using Rates.ViewModels.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Xaml;
//using Xceed.Wpf.Toolkit;

namespace Rates.ViewModels
{
    public class ShellViewModel : Conductor<object>,
        IHandle<CompanyChangeEvent>,
        IHandle<WindowSizeChangeEvent>,
        IHandle<CompaniesUpdate>
    {
        private IWindowManager _windowManager;
        private IEventAggregator _eventAggregator;
        private ICompanyService _companyService;
        private ICompanyRateService _companyRateService;
        private IRateService _rateService;

        private BindableCollection<CompanyModel> _companies;
        private CompanyModel _selectedCompany;
        private string _companyName;
        private int _numberOfDocuments;
        private string _errorMsg;
        private bool _detailsAreActive;
        private bool _isNotConstRate = true;
        private bool _saveAnnotationButtonIsEnabled;
        
        public ShellViewModel(
            IWindowManager windowManager,
            IEventAggregator eventAggregator,
            IRateService rateService,
            ICompanyService companyService)
        {
            _windowManager = windowManager;
            _eventAggregator = eventAggregator;
            _companyService = companyService;
            _rateService = rateService;
            eventAggregator.Subscribe(this);
            _companies = new BindableCollection<CompanyModel>(_companyService.GetAll());
        }

        public int NumberOfDocuments
        {
            get { return _numberOfDocuments; }
            set
            {
                _numberOfDocuments = value;
                NotifyOfPropertyChange(() => NumberOfDocuments);
                NotifyOfPropertyChange(() => Price);
                NotifyOfPropertyChange(() => ActualRange);
            }
        }
        public BindableCollection<CompanyModel> Companies
        {
            set
            {
                _companies =  value;
                NotifyOfPropertyChange(() => Companies);
            }
            get { return _companies; }
        }
        public CompanyModel SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                _selectedCompany = null;
                if (value != null)
                {
                    _selectedCompany = FindCompany(value.Name, _companyService);
                }

                if (_selectedCompany != null)
                {
                    _selectedCompany = FindCompany(_selectedCompany.Name, _companyService);
                    CompanyRateService = new CompanyRateService(_selectedCompany);
                    CompanyName = _selectedCompany.Name;
                    NotifyOfPropertyChange(() => CompanyName);
                    NotifyOfPropertyChange(() => SelectedCompany);
                    NotifyOfPropertyChange(() => Price);
                    NotifyOfPropertyChange(() => ActualRange);
                    NotifyOfPropertyChange(() => ExpanderInfoIsVisible);
                    NotifyOfPropertyChange(() => DetailsViewModel);
                    NotifyOfPropertyChange(() => IsNotConstRate);
                    Annotations = SelectedCompany.Annotations;
                    if (DetailsAreActive)
                    {
                        ReopenDetailsViewModel(SelectedCompany);
                    }
                }
            }
        }
        public double Price
        {
            get
            {
                RateModel rate = GetRate();
                return rate == null ? 0 : rate.Price;
            }
        }
        public string ActualRange
        {
            get
            {
                RateModel actualRate = GetRate();
                if (actualRate == null)
                {
                    return string.Empty;
                }
                if (actualRate.ConstRate)
                {
                    string constRate = "stała stawka";
                    return constRate;
                }
                else
                {
                    return new StringBuilder().
                    Append(actualRate.LowerLimit).
                    Append(" - ").
                    Append(actualRate.UpperLimit).ToString();
                }
            }
        }
        public bool ExpanderInfoIsVisible
        {
            get
            {
                return
                    SelectedCompany != null &&
                    !string.IsNullOrWhiteSpace(SelectedCompany.Annotations);
            }
        }
        public string DetailsButtonContent
        {
            get { return DetailsAreActive ? "Ukryj szczegóły" : "Pokaż szczegóły"; }
        }
        public bool IsNotConstRate
        {
            get { return _isNotConstRate; }
            set { _isNotConstRate = value; }
        }
        public bool DetailsAreActive
        {
            get { return _detailsAreActive; }
            set
            {
                _detailsAreActive = value;
                NotifyOfPropertyChange(() => DetailsAreActive);
                NotifyOfPropertyChange(() => DetailsButtonContent);
            }
        }

        private object detailsViewModel;
        private object addNextRateViewModel = null;
        private object addCompanyViewModel = null;
        public object DetailsViewModel
        {
            get { return detailsViewModel; }
            set { detailsViewModel = value; }
        }
        private void ReopenDetailsViewModel(CompanyModel model)
        {
            try
            {
                DetailsViewModel = new DetailsViewModel(SelectedCompany, _companyService, _eventAggregator, _windowManager, _rateService);
                WindowHeight = HeightMax;
                ActivateItem(DetailsViewModel);
                DetailsAreActive = true;
            }
            catch (CompanyException ce)
            {
                ErrorMsg = ce.Message;
            }
        }
        public void Details()
        {
            if (DetailsAreActive && DetailsViewModel != null)
            {
                DeactivateItem(DetailsViewModel, true);
                WindowHeight = HeightMin;
                DetailsAreActive = false;
            }
            else
            {
                ReopenDetailsViewModel(SelectedCompany);
            }
        }
        public void AddRates()
        {

            if (addNextRateViewModel != null)
            {
                ((Screen)addNextRateViewModel).TryClose();
            }
            try
            {
                addNextRateViewModel = new AddNextRateViewModel(
                     //_selectedCompany,
                     CompanyRateService,
                    _eventAggregator,
                    _windowManager,
                    _rateService);
                _windowManager.ShowWindow(addNextRateViewModel);
            }
            catch (CompanyException ce)
            {
                ErrorMsg = ce.Message;
            }
        }
        public void AddCompany()
        {
            //if (addCompanyViewModel != null)
            //{
            //    DeactivateItem(addCompanyViewModel, true);
            //    addCompanyViewModel = null;
            //    WindowHeight = 220;
            //    return;
            //}
            try
            {
                WindowHeight = 410;
                addCompanyViewModel = new AddCompanyViewModel(_eventAggregator, _windowManager, _companyService, _rateService, CompanyRateService);
                DetailsAreActive = false;
                ActivateItem(addCompanyViewModel);
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
        }

        public void Handle(CompanyChangeEvent companyChangeEvent)
        {
            Companies = new BindableCollection<CompanyModel>(_companyService.GetAll());
            if (_companyService.GetAll().Contains(companyChangeEvent.Company))
            {
                SelectedCompany = companyChangeEvent.Company;
                ReopenDetailsViewModel(SelectedCompany);
            }

        }
        private RateModel GetRate()
        {
            ErrorMsg = string.Empty;

            try
            {
                return SelectedCompany != null ? CompanyRateService.
                    FindRate(NumberOfDocuments) :
                    null;
            }
            catch (RateException re)
            {
                ErrorMsg = re.Message;
                return null;
            }
        }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                if (_isExpanded)
                {
                    WindowWidth = WidthMax;
                }
                else
                {
                    WindowWidth = WidthMin;
                }
            }
        }
        public string ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                _errorMsg = value;
                NotifyOfPropertyChange(() => ErrorMsg);
            }
        }
        //how to resolve window expanding problem

        private CompanyModel FindCompany(string name, ICompanyService _companyService)
        {
            try
            {
                return _companyService.FindCompany(name);
            }
            catch (Exception e)
            {

                ErrorMsg = e.Message;
            }

            return null;
        }

        public void Handle(WindowSizeChangeEvent message)
        {
            IsExpanded = true;
        }
       
        public void Handle(CompaniesUpdate message)
        {
            Companies = new BindableCollection<CompanyModel>(_companyService.GetAll());
            SelectedCompany = null;
        }

        internal ICompanyRateService CompanyRateService
        {
            get => _companyRateService;
            set { _companyRateService = value; }
        }

        public string CompanyName
        {
            get => _companyName;
            set
            {
                _companyName = value;
                NotifyOfPropertyChange(() => CompanyName);
            }
        }

        // Annotations
        private string _annotations;
        public string Annotations
        {
            get
            {
                return _annotations;
            }
            set
            {
                _annotations = value;
                NotifyOfPropertyChange(() => Annotations);
            }
        }
        public void SaveAnnotation()
        {
            //saving
            _companyService.UpdateCompanyAnnotations(CompanyName, Annotations);
            SaveAnnotationButtonIsEnabled = false;
        }
        public int AnnotationsRTBHeight
        {
            get
            {
                return WindowHeight - 140;
            }
        }
        public bool SaveAnnotationButtonIsEnabled
        {
            get { return _saveAnnotationButtonIsEnabled; }
            set
            {
                _saveAnnotationButtonIsEnabled = value;
                NotifyOfPropertyChange(() => SaveAnnotationButtonIsEnabled);
            }
        }
        public void TextChangeAction()
        {
            SaveAnnotationButtonIsEnabled = true;
        }


        // Window size
        private int _windowWidth = WidthMin;//??
        public int WindowWidth
        {
            get { return _windowWidth; }
            set
            {
                _windowWidth = value;
                NotifyOfPropertyChange(() => WindowWidth);
            }
        }
        private int _windowHeight = HeightMin;
        public int WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                _windowHeight = value;
                NotifyOfPropertyChange(() => WindowHeight);
                NotifyOfPropertyChange(() => AnnotationsRTBHeight);
            }
        }
        private static int WidthMax
        {
            get { return 520; }
        }
        private static int WidthMin
        {
            get { return 314; }
        }
        private static int HeightMax
        {
            get { return 410; }
        }
        private static int HeightMin
        {
            get { return 220; }
        }
    }
}
