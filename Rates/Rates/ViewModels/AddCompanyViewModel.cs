﻿using Caliburn.Micro;
using Rates.Models;
using Rates.Service;
using Rates.Service.impl;
using System;

namespace Rates.ViewModels
{
    class AddCompanyViewModel : Screen  
    {
        private IEventAggregator eventAggregator;
        private readonly IWindowManager windowManager;

        private ICompanyService companyService;
        private ICompanyRateService _companyRateService;
        private IRateService rateService;
        private string _companyName;
        private string _annotations;
        private string _errorMsg;
        private CompanyModel _createdCompany;

        public AddCompanyViewModel(
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            ICompanyService companyService,
            IRateService rateService,
            ICompanyRateService companyRateService)
        {
            CreatedCompany = new CompanyModel(CompanyName);
            this.eventAggregator = eventAggregator;
            this.windowManager = windowManager;
            this.rateService = rateService;
            _companyRateService = companyRateService;
            this.companyService = companyService;
        }
        public string ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                _errorMsg = value;
                NotifyOfPropertyChange(() => ErrorMsg);
            }
        }
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }
        public string Annotations
        {
            get { return _annotations; }
            set
            {
                _annotations = value;
                NotifyOfPropertyChange(() => Annotations);
                CreatedCompany.Annotations =Annotations;
            }
        }

        public CompanyModel CreatedCompany {
            get => _createdCompany;
            set => _createdCompany = value;
        }

        public void AddCompany()
        {
            try
            {
                CompanyModel company = companyService.AddCompany(CompanyName, CreatedCompany.Rates, Annotations);
                eventAggregator.PublishOnCurrentThread(new CompanyChangeEvent(company));
                TryClose();
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
        }
        public void AddRates()
        {
            _companyRateService = new CompanyRateService(CreatedCompany);
            windowManager.ShowWindow(
                new AddRatesViewModel(
                    CreatedCompany,
                    _companyRateService,
                    companyService,
                    eventAggregator,
                    windowManager,
                    rateService));
        }

        public void CloseScreen()
        {
            TryClose();
        }
    }
}
