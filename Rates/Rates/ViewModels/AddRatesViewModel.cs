﻿using Caliburn.Micro;
using Rates.Models;
using Rates.Service;
using Rates.Service.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Rates.ViewModels
{
    class AddRatesViewModel : Conductor<object>, IHandle<CompanyChangeEvent>
    {
        private IEventAggregator _eventAggregator;
        private IWindowManager _windowManager;

        private ICompanyRateService _companyRateService;
        private IRateService _rateService;
        private ICompanyService _companyService;
        private CompanyModel _createdCompany;
        private BindableCollection<CompanyModel> _companies;
        private CompanyModel _selectedCompany;
        
        private IList<RateModel> _rates;
        private string _errorMsg = string.Empty;


        private bool _editMode;
        private bool _constRate;

        private string companyName_;

        public string CompanyName
        {
            get { return companyName_; }
            set { companyName_ = value; }
        }


        public string ComboBoxText
        {
            get => "Stawki jak u..."; set
            { NotifyOfPropertyChange(() => ComboBoxText); }
        }



        public AddRatesViewModel(CompanyModel createdCompany,
            ICompanyRateService companyRateService,
            ICompanyService companyService,
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            IRateService rateService)
        {
            eventAggregator.Subscribe(this);
            this._companyService = companyService;
            this._createdCompany = createdCompany;
            this._companyRateService = companyRateService;
            this._eventAggregator = eventAggregator;
            this._windowManager = windowManager;
            this._rateService = rateService;
            this.CompanyName = _createdCompany.Name;
            Rates = _createdCompany.Rates;
            Companies = new BindableCollection<CompanyModel>(companyService.GetAll());
            if (Companies.Contains(_createdCompany))
            {
                _editMode = true;
            }
        }
        public void AddRate()
        {
            _windowManager.ShowWindow(new AddNextRateViewModel(_companyRateService, _eventAggregator, _windowManager, _rateService));
        }
        public void ClearRates()
        {
            _companyRateService.DeleteRates();
            Rates = new List<RateModel>(_companyRateService.GetAllRates());
            Companies = null;
            ComboBoxText = "Stawki jak u...";
            Companies = new BindableCollection<CompanyModel>(_companyService.GetAll());
        }
        public void Save()
        {
            try
            {
                _companyRateService.AddOrUpdateRates(Rates);

                _createdCompany.Rates = Rates;
                if (_editMode)
                {
                   
                    _companyService.UpdateCompanyRates(_createdCompany.Name, _createdCompany.Rates);
                    _eventAggregator.PublishOnCurrentThread(new CompanyChangeEvent(_createdCompany));
                }
                
                TryClose();
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }

        }

        public void Handle(CompanyChangeEvent companyChangeEvent)
        {
            Rates = new List<RateModel>(companyChangeEvent.Company.Rates);
        }



        public bool ConstRate
        {
            get { return _constRate; }
            set
            {
                if (Rates.Count > 0 && Rates[0].ConstRate)
                {
                    _constRate = true;
                }
                else
                {
                    _constRate = value;
                }
                NotifyOfPropertyChange(() => ConstRate);
            }
        }
        public BindableCollection<CompanyModel> Companies
        {
            set
            {
                _companies = value;
                NotifyOfPropertyChange(() => Companies);
            }
            get { return _companies; }
        }
        public CompanyModel SelectedCompany
        {
            get { return _selectedCompany; }
            set
            {
                if (value != null)
                {
                    try
                    {
                        //_selectedCompany is clone
                        _selectedCompany = _companyService.FindCompany(value.Name);
                        _createdCompany.Rates = SelectedCompany.Rates;
                        Rates = _createdCompany.Rates;
                        _companyRateService = new CompanyRateService(_createdCompany);
                        NotifyOfPropertyChange(() => SelectedCompany);
                        NotifyOfPropertyChange(() => ConstRate);
                        // _createdCompany.Rates= ratesSelectedCompany;
                        //_companyRateService.AddOrUpdateRegularRates(ratesSelectedCompany);
                    }

                    catch (Exception e)
                    {
                        ErrorMsg = e.Message;
                    }
                }

            }
        }
        public IList<RateModel> Rates
        {
            get { return _rates; }
            set
            {
                _rates = value;
                NotifyOfPropertyChange(() => Rates);
            }
        }
        public string ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                _errorMsg = value;
                NotifyOfPropertyChange(() => ErrorMsg);
            }
        }
    }
}
