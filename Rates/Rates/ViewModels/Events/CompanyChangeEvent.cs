﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.ViewModels
{
    public class CompanyChangeEvent
    {
        private CompanyModel company;
        public CompanyChangeEvent(CompanyModel company)
        {
            this.company = company;
        }

        public CompanyModel Company { get => company; }
    }
}
