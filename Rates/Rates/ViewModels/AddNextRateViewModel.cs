﻿using Rates.Exceptions;
using Rates.Models;
using System;
using System.Collections.Generic;
using Caliburn.Micro;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rates.Service;
using System.Windows;
using Rates.Service.impl;

namespace Rates.ViewModels
{
    class AddNextRateViewModel : Screen
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IWindowManager _windowManager;

        private IRateService _rateService;
        //private ICompanyService _companyService;

        //Company data
        CompanyModel _companyModel;
        private String _companyName;
        private bool _constRate;
        private double _price;
        private int _lowerLimit;
        private int _upperLimit;
        private string _errorMsg;
        private ICompanyRateService _companyRateService;

        public string ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                _errorMsg = value;
                NotifyOfPropertyChange(() => ErrorMsg);
            }
        }
        public double Price 
        {
            get { return _price; }
            set { _price = value;
                NotifyOfPropertyChange(() => Price);
            }
        }

        public int LowerLimit
        {
            get { return _lowerLimit; }
            set { _lowerLimit = value;
                NotifyOfPropertyChange(() => LowerLimit);
            }
        }
        public int UpperLimit { get { return _upperLimit; } set
            {
                _upperLimit = value;
                NotifyOfPropertyChange(() => UpperLimit);
            } }

        private enum AddRateOption{
            CRPM,// Const Rate Price Modification
            CRRR,// Const Rate on Regular Rate
            RRCR,// Regular Rate on Const Rate 
            RRNR// Regular Rate add Next Rate
        }

        public void AddRate()
        {
            ErrorMsg = string.Empty;
            try
            {
                RateModel rate = ConstRate ?
              _rateService.CreateConstRate(Price) :
              _rateService.CreateRegularRate(LowerLimit, UpperLimit, Price);

                AddRateOption option = specifyOption(rate, _companyModel);
                switch (option)
                {
                    case AddRateOption.CRPM:
                        _companyRateService.AddOrUpdateConstRate(rate);
                        break;
                    case AddRateOption.CRRR:
                        _companyRateService.DeleteRates();
                        _companyRateService.AddNextRegularRate(rate);
                        break;
                    case AddRateOption.RRCR:
                        //dodać ostrzeżenie o usunięciu stawek;
                        _companyRateService.DeleteRates();
                        _companyRateService.AddOrUpdateConstRate(rate);
                        break;
                    case AddRateOption.RRNR:
                        _companyRateService.AddNextRegularRate(rate);
                        break;
                    default:
                        break;
                }
                _eventAggregator.PublishOnCurrentThread(new CompanyChangeEvent(_companyModel)) ;
                if (IsNotConstRate)
                {
                    ReOpen();
                }
                else
                {
                    TryClose();
                }
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
          
        }
        private void ReOpen()
        {
            SetValues(_companyRateService.GetAllRates());
        }

        private AddRateOption specifyOption(RateModel rateModel, CompanyModel _companyModel)
        {
            IList<RateModel> rates = _companyModel.Rates;

            if (rates.Count==0)
            {
                if (rateModel.ConstRate)
                {
                    //addConstRate();
                    return AddRateOption.CRPM;
                }
                else
                {
                    //addRegularRate();
                    return AddRateOption.RRNR;
                }

            }
            RateModel previousRate = rates[0];
            if (rateModel.ConstRate&& previousRate.ConstRate)
            {
                //Firma ma stawkę stałą, zmiana kwoty 
                return AddRateOption.CRPM;
            }
            if (rateModel.ConstRate&&!previousRate.ConstRate)
            {
                //Firma ma stawki regularne, zmiana na stawkę stałą - uwaga usunięcie listy stawek 
                return AddRateOption.RRCR;
            }
            if (!rateModel.ConstRate&&previousRate.ConstRate)
            {
                //Firma ma stawkę stałą zmiana na stawki regularne
                return AddRateOption.CRRR;
            }
            return AddRateOption.RRNR;

        }

        public AddNextRateViewModel(
            ICompanyRateService companyRateService,
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            IRateService rateService)
        {

            _companyModel = companyRateService.GetCompany()?? throw new CompanyException(ExceptionMsg._errMsg4);

            _companyRateService = companyRateService;
            //_companyService = companyService;
            _eventAggregator = eventAggregator;
            _windowManager = windowManager;
            _rateService = rateService;


            CompanyName = _companyModel.Name;
            IList<RateModel> rates = _companyModel.Rates;
            SetValues(rates);
        }

        private void SetValues(IList<RateModel> rates)
        {
            if (rates.Count==0)
            {
                LowerLimit = 1;
                return;
            }
            ConstRate = rates[0].ConstRate;
            if (ConstRate)
            {
                Price = rates[0].Price;
            }
            else
            {
                LowerLimit = rates[rates.Count - 1].UpperLimit+1;
                UpperLimit = 0;
                Price = 0;
            }
        }
        public String CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public bool IsNotConstRate { get { return !ConstRate; } }
      
        public bool ConstRate
        {
            get { return _constRate; }
            set { _constRate = value;
                NotifyOfPropertyChange(() => ConstRate);
                NotifyOfPropertyChange(() => IsNotConstRate);
            }
        }
    }
}
