﻿using Caliburn.Micro;
using Rates.Exceptions;
using Rates.Models;
using Rates.Service;
using Rates.Service.impl;
using Rates.ViewModels.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Rates.ViewModels
{
    class DetailsViewModel : Screen
    {
        private ICompanyService _companyService;
        private IRateService _rateService;
        private ICompanyRateService _companyRateService;
        private IWindowManager _windowManager;
        private IEventAggregator _eventAggregator;

        private CompanyModel SelectedCompany { get; }
        private String _companyName;

        private bool _saveChangesButtonShouldBeAvtive;

        public String CompanyName
        {
            get { return _companyName; }
            set { _companyName = value;
                NotifyOfPropertyChange(() => CompanyName);
            }
        }

        private IList<RateModel> _rates;
        private string _errorMsg;

        public DetailsViewModel(CompanyModel selectedCompany,
            ICompanyService companyService,
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            IRateService rateService)
        {
            _companyService = companyService;
            _rateService = rateService;
            _companyService = companyService;
            SelectedCompany = selectedCompany ?? throw new CompanyException(ExceptionMsg._errMsg2);
            SelectedCompany = _companyService.FindCompany(selectedCompany.Name);

            CompanyName = SelectedCompany.Name;
            Rates = selectedCompany.Rates;

            _eventAggregator = eventAggregator;
            _windowManager = windowManager;

            _companyRateService = new CompanyRateService(selectedCompany);

        }

        public void RatesEditEnding()
        {

            try
            {
                if (_companyRateService.RatesValidator(Rates))
                {
                    _companyRateService.AddOrUpdateRates(Rates);
                    SaveChangesButtonShouldBeAvtive = true;

                }
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }

            //try
            //{
            //    _companyRateService.RatesValidator(_createdCompany.Rates);

            //    _createdCompany.Rates = Rates;
            //    if (_editMode)
            //    {
            //        _companyService.UpdateCompany(_createdCompany.Name, _createdCompany);
            //        _eventAggregator.PublishOnCurrentThread(new CompanyChangeEvent(_createdCompany));
            //    }
            //    TryClose();
            //}
            //catch (Exception e)
            //{
            //    ErrorMsg = e.Message;
            //}

        }


        public void RatesEditBeginning()
        {
            try
            {
                ErrorMsg = string.Empty;
                SaveChangesButtonShouldBeAvtive = true;
                _companyRateService.RatesValidator(Rates);
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
        }
        public void SaveChanges()
        {
            try
            {
                _companyRateService.RatesValidator(Rates);
                SelectedCompany.Rates = Rates;
                _companyService.UpdateCompanyRates(CompanyName, Rates);
                ErrorMsg = string.Empty;
                SaveChangesButtonShouldBeAvtive = false;
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
        }

        public void ChangeRates()
        {
            try
            {
                _windowManager.ShowWindow(
                    new AddRatesViewModel(
                        SelectedCompany,
                        _companyRateService,
                        _companyService,
                        _eventAggregator,
                        _windowManager,
                        _rateService));
            }
            catch (Exception e)
            {
                ErrorMsg = e.Message;
            }
        }
        public void AnnotationsEdit()
        {
            _eventAggregator.PublishOnCurrentThread(new WindowSizeChangeEvent());
        }

        public IList<RateModel> Rates
        {
            get { return _rates; }
            set
            {
                _rates = value;
                NotifyOfPropertyChange(() => Rates);
            }
        }
        public bool SaveChangesButtonShouldBeAvtive
        {
            get { return _saveChangesButtonShouldBeAvtive; }
            set
            {
                _saveChangesButtonShouldBeAvtive = value;
                NotifyOfPropertyChange(() => SaveChangesButtonShouldBeAvtive);
            }
        }

        public string ErrorMsg
        {
            get { return _errorMsg; }
            set
            {
                _errorMsg = value;
                NotifyOfPropertyChange(() => ErrorMsg);
            }
        }
        public void DeleteCompany()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Czy na pewno usunąć tę firmę?", "Potwierdzenie usunięcia firmy", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                _companyService.DeleteCompany(SelectedCompany.Name);
                Rates = null;
                CompanyName = string.Empty;
                _eventAggregator.PublishOnCurrentThread(new CompaniesUpdate());

            }
        }
    }
}
