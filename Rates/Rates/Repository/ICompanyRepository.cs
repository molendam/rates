﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Repository
{
    interface ICompanyRepository
    {
        CompanyModel AddCompany(string name, IList<RateModel> rates, string annotations);
        void DeleteCompany(string name);
        CompanyModel FindCompany(string name);
        IList<CompanyModel> GetAll();
        void AddCompanyRate(RateModel rate, string name);
    }
}
