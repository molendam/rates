﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rates.Exceptions;
using Rates.Models;
using Rates.Repository.testData;

namespace Rates.Repository.impl
{
    class CompanyRepository : ICompanyRepository
    {
        private IList<CompanyModel> companies = new List<CompanyModel>();
        public CompanyRepository()
        {
            companies = CompaniesTestData.Companies;
        }

        public CompanyModel AddCompany(string name, IList<RateModel> rates, string annotations)
        {
            if (FindCompany(name)!=null)
            {
                return null;
            }
            CompanyModel company = new CompanyModel(name);
            company.Annotations = annotations;
            company.Rates = rates;
            companies.Add(company);
            return company;

        }

        public void AddCompanyRate(RateModel rate, string name)
        {
            CompanyModel company = FindCompany(name);
            if (company != null)
            {
                company.Rates.Add(rate);
            }
        }

        public void DeleteCompany(string name)
        {
            CompanyModel company = FindCompany(name);
            if (company !=null)
            {
                companies.Remove(company);
            }
        }

        public CompanyModel FindCompany(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            foreach (CompanyModel company in companies)
            {
                if (company.Name.ToLower() == name.ToLower())
                {
                    return company;
                }
            }
            return null;
        }

        public IList<CompanyModel> GetAll()
        {
            return companies;
        }
    }
}
