﻿using Rates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Rates.Repository.testData
{
    static class CompaniesTestData
    {

        public static IList<CompanyModel> Companies
        {
            get { return createCompanies(); }
        }

        private static IList<CompanyModel> createCompanies()
        {
            List<CompanyModel> companies = new List<CompanyModel>();

            CompanyModel cm1 = new CompanyModel("Agencja B.R. Racz s.c.");
            CompanyModel cm2 = new CompanyModel("EXTOR Sp. z o.o.");
            CompanyModel cm3 = new CompanyModel("R-Wood Robert Firszt");
            CompanyModel cm4 = new CompanyModel("Evergrip Sp. z o.o.");
            CompanyModel cm5 = new CompanyModel("Baltic Ground Services PL Sp.z.o.o.");
            CompanyModel cm6 = new CompanyModel("Baśniowy Ogród");


            cm1.Rates.Add(new RateModel(1, 10, 100.50));
            cm1.Rates.Add(new RateModel(11, 20, 200.32));
            cm1.Rates.Add(new RateModel(21, 30, 300.45));
            cm1.Rates.Add(new RateModel(31, 60, 450));
            cm1.Rates.Add(new RateModel(60, 130, 600));
            cm1.Rates.Add(new RateModel(21, 200, 800));

            cm2.Rates.Add(new RateModel(132));

            FlowDocument flow = new FlowDocument();
            flow.Blocks.Add(new Paragraph(new Run( "text context")));

        
            cm2.Annotations = "<Section xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" xml:space=\"preserve\" TextAlignment=\"Left\" LineHeight=\"2\" IsHyphenationEnabled=\"False\" xml:lang=\"en-us\" FlowDirection=\"LeftToRight\" NumberSubstitution.CultureSource=\"User\" NumberSubstitution.Substitution=\"AsCulture\" FontFamily=\"Segoe UI\" FontStyle=\"Normal\" FontWeight=\"Normal\" FontStretch=\"Normal\" FontSize=\"12\" Foreground=\"#FF000000\" Typography.StandardLigatures=\"True\" Typography.ContextualLigatures=\"True\" Typography.DiscretionaryLigatures=\"False\" Typography.HistoricalLigatures=\"False\" Typography.AnnotationAlternates=\"0\" Typography.ContextualAlternates=\"True\" Typography.HistoricalForms=\"False\" Typography.Kerning=\"True\" Typography.CapitalSpacing=\"False\" Typography.CaseSensitiveForms=\"False\" Typography.StylisticSet1=\"False\" Typography.StylisticSet2=\"False\" Typography.StylisticSet3=\"False\" Typography.StylisticSet4=\"False\" Typography.StylisticSet5=\"False\" Typography.StylisticSet6=\"False\" Typography.StylisticSet7=\"False\" Typography.StylisticSet8=\"False\" Typography.StylisticSet9=\"False\" Typography.StylisticSet10=\"False\" Typography.StylisticSet11=\"False\" Typography.StylisticSet12=\"False\" Typography.StylisticSet13=\"False\" Typography.StylisticSet14=\"False\" Typography.StylisticSet15=\"False\" Typography.StylisticSet16=\"False\" Typography.StylisticSet17=\"False\" Typography.StylisticSet18=\"False\" Typography.StylisticSet19=\"False\" Typography.StylisticSet20=\"False\" Typography.Fraction=\"Normal\" Typography.SlashedZero=\"False\" Typography.MathematicalGreek=\"False\" Typography.EastAsianExpertForms=\"False\" Typography.Variants=\"Normal\" Typography.Capitals=\"Normal\" Typography.NumeralStyle=\"Normal\" Typography.NumeralAlignment=\"Normal\" Typography.EastAsianWidths=\"Normal\" Typography.EastAsianLanguage=\"Normal\" Typography.StandardSwashes=\"0\" Typography.ContextualSwashes=\"0\" Typography.StylisticAlternates=\"0\"><Paragraph><Run xml:lang=\"pl-pl\" FontWeight=\"Bold\" TextDecorations=\"Underline\">Title</Run></Paragraph><List MarkerStyle=\"Decimal\"><ListItem><Paragraph><Run xml:lang=\"pl-pl\">Questions here at RenewableEnergyabout who the big.</Run></Paragraph></ListItem><ListItem><Paragraph><Run xml:lang=\"pl-pl\">Players in each sector of the </Run><Run xml:lang=\"pl-pl\" Foreground=\"#FFFF0000\">renewable energy </Run><Run xml:lang=\"pl-pl\">industry are, so we. </Run></Paragraph></ListItem><ListItem><Paragraph><Run xml:lang=\"pl-pl\">Decided to start.</Run></Paragraph></ListItem></List></Section>";

            companies.Add(cm1);
            companies.Add(cm2);
            companies.Add(cm3);
            return companies;
        }

    }
}
