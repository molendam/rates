﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Rates.Models
{
    public class CompanyModel: ICloneable, IComparable /*INotifyPropertyChanged*/
    {
        private IList<RateModel> rates = new List<RateModel>();
        private string name;
        private string annotations;


        public CompanyModel(string name)
        {
            //PropertyChanged?.Invoke(this, null);
            this.name = name;
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public IList<RateModel> Rates { get => rates; set => rates = value; }
        public string Annotations { get { return annotations; } set => annotations = value; }

        public object Clone()
        {
            CompanyModel companyClone = (CompanyModel)MemberwiseClone();
            companyClone.Rates =  rates.ToList().ConvertAll(rate =>(RateModel) rate.Clone());
            
            return companyClone;
        }

        public int CompareTo(object obj)
        {
            var model = obj as CompanyModel;
            
            return this.Name.CompareTo(model.Name);
        }

        public override bool Equals(object obj)
        {
            var model = obj as CompanyModel;
            return model != null &&
                   name == model.name &&
                   Name == model.Name;
        }

        public override int GetHashCode()
        {
            var hashCode = 629881564;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }

        //public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return name;
        }
        
       
    }
}
