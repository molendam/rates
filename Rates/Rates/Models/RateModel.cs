﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Models
{
    public class RateModel : ICloneable
    {
        private bool constRate;
        private double price;
        private int lowerLimit;
        private int upperLimit;

        public RateModel(int lowerLimit, int upperLimit, double price)
        {
            this.lowerLimit = lowerLimit;
            this.upperLimit = upperLimit;
            this.price = price;
        }
        public RateModel(double price)
        {
            constRate = true;
            this.price = price;
        }
        public int LowerLimit
        {
            get { return lowerLimit; }
            set { lowerLimit = value; }
        }
        public int UpperLimit
        {
            get { return upperLimit; }
            set { upperLimit = value; }
        }
        public double Price { get => price; set => price = value; }
        public bool ConstRate { get => constRate; set => constRate = value; }
        public override string ToString()
        {
            return new StringBuilder()
                .Append("LOWER LIMIT: ")
                .Append(LowerLimit)
                .Append(" UPPER LIMIT: ")
                .Append(UpperLimit)
                .Append(" PRICE: ")
                .Append(price).ToString();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
