﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Exceptions
{
    class CompanyException : Exception
    {
        public override string Message => errMsg;
        private string errMsg;
        public CompanyException(string errMsg)
        {
            this.errMsg = errMsg;
        }
    }
}
