﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Exceptions
{
    static class ExceptionMsg
    {
        public static string _errMsg1 => "Taka stawka nie istnieje";
        public static string _errMsg2 => "Taka firma nie istnieje";
        public static string _errMsg3 => "Taka firma już istnieje";
        public static string _errMsg4 => "Wybierz firmę";
        public static string _errMsg5 => "Nazwa firmy nie może być pusta";
        public static string _errMsg6 => "Górny zakres nie może być mniejszy od dolnego";
        public static string _errMsg7 => "Cena nie może być mniejsza bądź równa 0";
        public static string _errMsg8 => "Stawka nachodzi na zakres poprzedniej";
        public static string _errMsg9 => "Nie udało się stworzyć stawki";
        public static string _errMsg10 => "Dodanie stawek nie powiodło się";
        public static string _errMsg11 => "Podane stawki są nieprawidłowe";
        public static string _errMsg12 => "Aby dodać firmę uzupełnij stawki";
        public static string _errMsg13 => "Zakres nie może być mniejszy bądź równy 0";
        public static string _errMsg14 => "Stawka stała nie może mieć zakresu, edytuj stawki";


    }
}
