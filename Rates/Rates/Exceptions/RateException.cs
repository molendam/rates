﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rates.Exceptions
{
    public class RateException : Exception
    {
        public override string Message => errMsg;
        private string errMsg;

        public RateException(string errMsg)
        {
            this.errMsg= errMsg;
        }

    }
}
